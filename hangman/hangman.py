import random


class Hangman:
    def __init__(self):
        self.game_state = None
        self.words = None
        self.current_word = None
        self.hidden_word = None
        self.tries = 8
        self.tried_letters = set()

    def set_words(self, words):
        self.words = list(words)

    def select_word(self):
        self.current_word = random.choice(self.words)

    def set_hidden_word(self):
        self.hidden_word = "-" * len(self.current_word)

    def reveal_letter(self, letter):
        revealed_word = self.hidden_word

        for index, i in enumerate(self.current_word):
            if i == letter:
                revealed_word = revealed_word[:index] \
                                + letter \
                                + revealed_word[index + 1:]

        self.hidden_word = revealed_word

    def add_to_tried_letters(self, letter):
        self.tried_letters.add(letter)

    def decrement_tries(self, amount):
        self.tries -= amount

    def guess(self):
        print("")
        print(self.hidden_word)

        letter = input("Input a letter:")

        if len(letter) != 1:
            print("You should input a single letter")
        elif not self.is_ascii_lower(letter):
            print("It is not an ASCII lowercase letter")
        elif letter in self.tried_letters:
            print("You already typed this letter")
        elif letter in self.current_word \
                and letter not in self.tried_letters:
            self.reveal_letter(letter)
            self.add_to_tried_letters(letter)
        else:
            print("No such letter in the word")
            self.decrement_tries(1)
            self.add_to_tried_letters(letter)

    def menu(self):
        user_action = input("Type \"play\" to play the game, \"exit\" to quit: ")

        if user_action == 'exit':
            self.game_state = 'exit'
        else:
            self.game_state = 'play'

    def play(self):
        self.select_word()
        self.set_hidden_word()

        self.welcome_message()
        self.menu()

        while self.tries >= 0:
            if self.game_state == 'exit':
                break

            if self.current_word == self.hidden_word:
                self.win_message()
                break

            if self.tries <= 0 and self.current_word != self.hidden_word:
                self.lose_message()
                break

            self.guess()

    @staticmethod
    def is_ascii_lower(letter):
        if 97 <= ord(letter) <= 122:
            return True

        return False

    @staticmethod
    def welcome_message():
        print("H A N G M A N")

    @staticmethod
    def lose_message():
        print("You are hanged!")

    @staticmethod
    def win_message():
        print("You guessed the word!")
        print("You survived!")


hangman = Hangman()
hangman.set_words(["python", "java", "kotlin", "javascript"])
hangman.play()
